// stdlib functionality
#include <iostream>
// ROOT functionality
#include <TFile.h>
#include <TArray.h>
#include <TH2.h>
#include <TStyle.h>
#include <TCanvas.h>
// ATLAS EDM functionality
#include "xAODRootAccess/Init.h"
#include "xAODRootAccess/TEvent.h"
#include "xAODEventInfo/EventInfo.h"
#include "xAODJet/JetContainer.h"

// Add a submodule for JetSelectionHelper
#include "JetSelectionHelper/JetSelectionHelper.h"

int main() {

  // initialize the xAOD EDM
  xAOD::Init();

  // open the input file
  TString inputFilePath = "/home/atlas/Bootcamp/Data/DAOD_EXOT27.17882744._000026.pool.root.1";
  xAOD::TEvent event;
  std::unique_ptr< TFile > iFile ( TFile::Open(inputFilePath, "READ") );
  event.readFrom( iFile.get() );

  // get the number of events in the file to loop over
  const Long64_t numEntries = event.getEntries();
  std::cout << "Number of Entries: " << numEntries << std::endl;

  // make arrays to plot
  TArrayD* njets = new TArrayD(numEntries);
  TH1* njetshist = new TH1D("njetshist", "Number of Jets", 20, 0, 20);
  TArrayD* massjets = new TArrayD(numEntries);
  TH1* massjetshist = new TH1D("massjetshist", "Dijet Invariant Mass", 100, 0, 500);
  TArrayD* njetscuts = new TArrayD(numEntries);
  TH1* njetscutshist = new TH1D("njetscutshist", "Number of Jets", 20, 0, 20);
  TArrayD* massjetscuts = new TArrayD(numEntries);
  TH1* massjetscutshist = new TH1D("massjetscutshist", "Dijet Invariant Mass", 100, 0, 500);

  // add jet selection helper
  JetSelectionHelper jet_selector;

  // primary event loop
  for ( Long64_t i=0; i<numEntries; ++i ) {
  //for ( Long64_t i=0; i<10; ++i ) {

    // Load the event
    event.getEntry( i );
    std::cout << "\r" << "Loading event " << i+1 << " of " << numEntries << "." << std::flush;

    /*
    // Load xAOD::EventInfo and print the event info
    const xAOD::EventInfo * ei = nullptr;
    event.retrieve( ei, "EventInfo" );
    std::cout << "Processing run # " << ei->runNumber() << ", event # " << ei->eventNumber() << std::endl;
    */

    // retrieve the jet container from the event store
    const xAOD::JetContainer* jets = nullptr;
    event.retrieve(jets, "AntiKt4EMTopoJets");

    // loop through all of the jets and make selections with the helper
    int jetcount = 0;
    int jetcountcuts = 0;
    TLorentzVector* dijet4mom = new TLorentzVector();
    TLorentzVector* dijet4momcuts = new TLorentzVector();
    for(const xAOD::Jet* jet : *jets) {
      // print the kinematics of each jet in the event
      //std::cout << "Jet : pt=" << jet->pt() << "  eta=" << jet->eta() << "  phi=" << jet->phi() << "  m=" << jet->m() << std::endl;

      if (jetcount < 2) {
        (*dijet4mom) += jet->p4();
      }
      jetcount++;

      if (std::abs(jet->eta()) < 2.5 and jet->pt() > 50000) {
        if (jetcountcuts < 2) {
          (*dijet4momcuts) += jet->p4();
        }
        jetcountcuts++;
      }

      if (jet_selector.isJetGood(jet)) {
        std::cout << "good";
      }

    }
    (*massjets)[i] = dijet4mom->M() / 1000.;
    (*massjetscuts)[i] = dijet4momcuts->M() / 1000.;
    //std::cout << "Dijet Invariant Mass : " << (*massjets)[i] << std::endl;
    massjetshist->Fill((*massjets)[i]);
    massjetscutshist->Fill((*massjetscuts)[i]);

    (*njets)[i] = jetcount;
    (*njetscuts)[i] = jetcountcuts;
    //std::cout << "Number of Jets : " << (*njets)[i] << std::endl;
    njetshist->Fill(jetcount);
    njetscutshist->Fill(jetcountcuts);

  }

  std::cout << std::endl;

  TCanvas* c1 = new TCanvas("c1","jet_n",700,500);
  c1->cd();
  njetshist->GetXaxis()->SetTitle("N Jets Per Event");
  njetshist->GetYaxis()->SetTitle("N Events / Bin");
  njetshist->Draw("hist");
  njetshist->Write();
  c1->Print("njets.pdf");

  TCanvas* c2 = new TCanvas("c2","jet_mass",700,500);
  c2->cd();
  massjetshist->GetXaxis()->SetTitle("Dijet Invariant Mass [GeV]");
  massjetshist->GetYaxis()->SetTitle("N Events / Bin");
  massjetshist->Draw("hist");
  massjetshist->Write();
  c2->Print("massjets.pdf");

  TCanvas* c3 = new TCanvas("c3","jet_n_with_cuts",700,500);
  c3->cd();
  njetscutshist->GetXaxis()->SetTitle("N Jets Per Event");
  njetscutshist->GetYaxis()->SetTitle("N Events / Bin");
  njetscutshist->Draw("hist");
  njetscutshist->Write();
  c3->Print("njets_cuts.pdf");

  TCanvas* c4 = new TCanvas("c4","jet_mass_with_cuts",700,500);
  c4->cd();
  massjetscutshist->GetXaxis()->SetTitle("Dijet Invariant Mass [GeV]");
  massjetscutshist->GetYaxis()->SetTitle("N Events / Bin");
  massjetscutshist->Draw("hist");
  massjetscutshist->Write();
  c4->Print("massjets_cuts.pdf");

  // exit from the main function cleanly
  return 0;
}
